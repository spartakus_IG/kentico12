﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.Dto.Home;
using Business.Services.Query;
using CMS.DocumentEngine.Types.MedioClinic;

namespace Business.Repository.Home
{
    public class HomeSectionRepository : BaseRepository, IHomeSectionRepository
    {
        public HomeSectionRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {
        }

        public HomeSectionDto GetHomeSection()
        {
            return DocumentQueryService.GetDocuments<HomeSection>()
                   .AddColumns("Title", "Text", "Button")  // Adds only specified columns to the columns selected in DocumentQueryService
                   .TopN(1)  // Selects exactly one record
                   .ToList()
                   .Select(m => new HomeSectionDto()  // Maps the selected values to the DTO's properties
                   {
                       Title = m.Title,
                       Text = m.Text,
                       LinkText = m.Button
                   })
                   .FirstOrDefault();
        }
    }
}