﻿using Business.Dto.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.Contact
{
    public interface IContactSectionRepository
    {
        ContactSectionDto GetContactSection();
    }
}
