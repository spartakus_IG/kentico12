﻿using Business.Dto.Contact;
using Business.Services.Query;
using CMS.DocumentEngine.Types.MedioClinic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.Contact
{

    class ContactSectionRepository : BaseRepository, IContactSectionRepository
    {

        public ContactSectionRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {
        }

        public ContactSectionDto GetContactSection()
        {
            return DocumentQueryService.GetDocuments<ContactSection>()
                .TopN(1)
                .AddColumns("Title", "Subtitle", "Text")
                .ToList()
                .Select(m => new ContactSectionDto()
                {
                    Header = m.Title,
                    Subheader = m.Subtitle,
                    Text = m.Text,
                })
                .FirstOrDefault();
        }
    }
}