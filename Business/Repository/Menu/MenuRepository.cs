﻿using System.Collections.Generic;
using System.Linq;
using CMS.DocumentEngine;
using Business.Dto.Menu;
using Business.Services.Query;
using CMS.DocumentEngine.Types.MedioClinic;

namespace Business.Repository.Menu
{
    public class MenuRepository : BaseRepository, IMenuRepository
    {
        public MenuRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {
        }

        public IEnumerable<MenuItemDto> GetMenuItems()
        {
            return DocumentQueryService.GetDocuments<MenuContainerItem>()
                .Path("/Menu-items", PathTypeEnum.Children) // Retrieves children items under specified path
                .AddColumns("Caption", "Controller", "Action")  // Adds only the specified columns
                .OrderByAscending("NodeOrder")  // Orders the items according to their order property
                .Select(m => new MenuItemDto()  // Maps the values to MenuItemDto
                {
                    Action = m.Action,
                    Caption = m.Caption,
                    Controller = m.Controller
                });
        }
    }
}
