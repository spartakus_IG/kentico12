﻿using Business.Dto.Map;
using System.Collections.Generic;

namespace Business.Repository.Map
{
    public interface IMapRepository : IRepository
    {
        IEnumerable<MapLocationDto> GetOfficeLocations();
    }
}