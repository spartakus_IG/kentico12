﻿using Business.Dto.Social;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.Social
{
    public interface ISocialLinkRepository : IRepository
    {
        IEnumerable<SocialLinkDto> GetSocialLinks();
    }
}
