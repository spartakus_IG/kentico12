﻿using Business.Repository;

namespace Business.Dto.Company
{
    public interface ICompanyRepository : IRepository
    {
        CompanyDto GetCompany();
    }
}
