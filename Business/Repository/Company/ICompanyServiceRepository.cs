﻿using Business.Dto.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.Company
{
    public interface ICompanyServiceRepository : IRepository
    {
        IEnumerable<CompanyServiceDto> GetCompanyServices();

    }
}
