﻿using Business.Dto.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.Culture
{
    public interface ICultureRepository : IRepository
    {
        IEnumerable<CultureDto> GetSiteCultures();

    }
}
