﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.Dto.Company;
using Business.Repository.Culture;
using Business.Repository.Menu;
using Business.Repository.Social;
using Business.Services.Context;

namespace Business.DependencyInjection
{
    public class BusinessDependencies : IBusinessDependencies
    {
        public IMenuRepository MenuRepository { get; }
        public ISiteContextService SiteContextService { get; }
        public ICompanyRepository CompanyRepository { get; }
        public ISocialLinkRepository SocialLinkRepository { get; }
        public ICultureRepository CultureRepository { get; }

        public BusinessDependencies(
              IMenuRepository menuRepository,
              ICompanyRepository companyRepository,
              ICultureRepository cultureRepository,
              ISiteContextService siteContextService,
              ISocialLinkRepository socialLinkRepository
              )
        {
            MenuRepository = menuRepository;
            CompanyRepository = companyRepository;
            CultureRepository = cultureRepository;
            SiteContextService = siteContextService;
            SocialLinkRepository = socialLinkRepository;
        }
    }
}