﻿using CMS.DocumentEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto.Social
{
    public class SocialLinkDto
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public DocumentAttachment Icon { get; set; }
    }
}