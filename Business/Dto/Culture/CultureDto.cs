﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto.Culture
{
    public class CultureDto : IDto
    {
        public string CultureCode { get; set; }
        public string CultureName { get; set; }
        public string CultureShortName { get; set; }
    }
}