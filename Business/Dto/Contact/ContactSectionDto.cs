﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dto.Contact
{
    public class ContactSectionDto : IDto
    {
        public string Header { get; set; }
        public string Subheader { get; set; }
        public string Text { get; set; }
    }
}
