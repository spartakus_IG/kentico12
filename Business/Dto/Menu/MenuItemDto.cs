﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dto.Menu
{
    public class MenuItemDto : IDto
    {
        public string Action { get; set; }
        public string Caption { get; set; }
        public string Controller { get; set; }
    }
}
