﻿using CMS.DocumentEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto.Company
{
    public class CompanyServiceDto : IDto
    {
        public string Text { get; set; }
        public DocumentAttachment Icon { get; set; }
        public string Header { get; set; }
    }
}