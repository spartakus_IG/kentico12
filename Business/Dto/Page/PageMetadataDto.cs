﻿using Business.Dto.Company;
using Business.Dto.Social;
using Business.Repository.Company;
using Business.Repository.Social;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto.Page
{
    public class PageMetadataDto : IDto
    {
        public string Title { get; set; }
        public string CompanyName { get; set; }
      
    }
}