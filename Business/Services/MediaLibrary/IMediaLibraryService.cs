﻿using Business.Dto.MediaLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services.MediaLibrary
{
    public interface IMediaLibraryService
    {
        IEnumerable<MediaLibraryFileDto> GetMediaLibraryFiles(string folder, string sitename, params string[] extensions);
    }
}
