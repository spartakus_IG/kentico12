﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MedioClinic.Utils
{
    public class MultiCultureMvcRouteHandler : MvcRouteHandler
    {
        public const string CultureUrlParam = "culture";
        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            try
            {

                // Gets the requested culture from the route
                var cultureName = requestContext.RouteData.Values[CultureUrlParam].ToString();

                // Creates a new CultureInfo object for the requested culture
                var culture = new CultureInfo(cultureName);

                // Sets the culture for the thread
                Thread.CurrentThread.CurrentUICulture = culture;
                Thread.CurrentThread.CurrentCulture = culture;
            }
            catch
            {
                // Returns a 404 response if the culture prefix is invalid
                requestContext.HttpContext.Response.StatusCode = 404;
            }

            return base.GetHttpHandler(requestContext);
        }
    }
}