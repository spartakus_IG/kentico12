﻿using CMS.SiteProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace MedioClinic.Utils
{
    public class SiteCultureConstraint : IRouteConstraint
    {
        private readonly HashSet<string> _allowedCultureNames;
        public SiteCultureConstraint(string siteName)
        {
            var siteCultureNames = CultureSiteInfoProvider.GetSiteCultureCodes(siteName);

            // Ignores letter case when comparing cultures
            _allowedCultureNames = new HashSet<string>(siteCultureNames, StringComparer.InvariantCultureIgnoreCase);
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var cultureName = values[parameterName].ToString();

            return _allowedCultureNames.Contains(cultureName);
        }
    }
}