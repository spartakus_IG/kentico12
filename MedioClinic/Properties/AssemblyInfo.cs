using System.Reflection;
using System.Runtime.InteropServices;

using CMS;

[assembly: AssemblyDiscoverable]

[assembly: AssemblyTitle("MedioClinic")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("4ae59fe8-59e3-4ba5-b705-52fbd47764ab")]

[assembly: AssemblyProduct("MedioClinic")]
[assembly: AssemblyCopyright("© 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersion("12.0.6900.17470")]
[assembly: AssemblyInformationalVersion("12.0.0")]
