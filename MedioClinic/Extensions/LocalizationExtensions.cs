﻿using CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedioClinic.Extensions
{
    public static class LocalizationExtensions
    {
        public static string Localize(this HtmlHelper helper, string key)
        {
            return ResHelper.GetString(key);
        }
    }
}