﻿using Business.Dto.Doctors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedioClinic.Models.Doctors
{
    public class DoctorDetailViewModel : IViewModel
    {
        public DoctorDto Doctor { get; set; }
    }
}