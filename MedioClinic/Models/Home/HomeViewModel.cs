﻿using Business.Dto.Company;
using Business.Dto.Home;
using System.Collections.Generic;

namespace MedioClinic.Models.Home
{
    public class HomeViewModel : IViewModel
    {
        public HomeSectionDto HomeSection { get; set; }
        public IEnumerable<CompanyServiceDto> CompanyServices { get; set; }

    }
}