﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using MedioClinic;

namespace MedioClinic.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/master-scripts")
                .IncludeDirectory("~/Scripts/Master", "*.js", true)
            );

            bundles.Add(new StyleBundle("~/bundles/master-css")
                .IncludeDirectory("~/Content/Css/Master", "*.css", true)
            );

            // Enables bundling and minification of bundle references
            BundleTable.EnableOptimizations = true;
        }
    }
}